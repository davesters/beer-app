import React from 'react';
import Parse from 'parse/react-native';
import { inject } from 'mobx-react';
import { Alert } from 'react-native';
import { NavigationScreenProp } from 'react-navigation';
import { Container, Content, Spinner } from 'native-base';

import RecipeStore from './stores/RecipeStore';

type Props = {
	navigation: NavigationScreenProp<{}>,
	recipeStore: RecipeStore,
};

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
class LoadingScreen extends React.Component<Props> {

	constructor(props: Props) {
		super(props);

		this.bootstrapAsync();
	}

	bootstrapAsync = () => {
		return Promise.resolve(Parse.User.currentAsync())
			.then(user => {
				if (user) {
					return this.props.recipeStore.load()
						.then(() => {
							this.props.navigation.navigate('App');
							return true;
						})
						.catch(err => {
							Alert.alert(
								'Error loading recipes',
								err.message,
								[
									{ text: 'OK' },
								],
							);
							this.props.navigation.navigate('App');
							return false;
						});
				}

				return this.props.navigation.navigate('Auth');
			})
			.catch(err => {
				Alert.alert(
					'Error loading user',
					err.message,
					[
						{ text: 'OK' },
					],
				);
				this.props.navigation.navigate('Auth');
				return false;
			});
	}

	render() {
		return (
			<Container>
				<Content>
					<Spinner color="blue" />
				</Content>
			</Container>
		);
	}
}

export default LoadingScreen;
