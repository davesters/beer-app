import React from 'react';
import Parse from 'parse/react-native';
import { Provider } from 'mobx-react';
import { AsyncStorage } from 'react-native';
import { StackNavigator, SwitchNavigator, DrawerNavigator } from 'react-navigation';

import RecipeStore from './stores/RecipeStore';
import Sidebar from './components/Sidebar';
import RecipeListScreen from './views/recipes/RecipeList';
import LoadingScreen from './Loading';
import SignInScreen from './views/auth/SignIn';
import ViewRecipe from './views/recipes/ViewRecipe';
import EditRecipe from './views/recipes/EditRecipe';
import NewRecipe from './views/recipes/NewRecipe';

const appNavigation = StackNavigator(
	{
		RecipeListScreen: { screen: RecipeListScreen },
		ViewRecipeScreen: { screen: ViewRecipe },
		EditRecipeScreen: { screen: EditRecipe },
		NewRecipeScreen: { screen: NewRecipe },
	},
	{
		initialRouteName: 'RecipeListScreen',
	},
);

const appDrawerNavigation = DrawerNavigator(
	{
		Main: appNavigation,
	},
	{
		contentComponent: props => <Sidebar rootNavigation={props.navigation} {...props} />,
		initialRouteName: 'Main',
	},
);

const authNavigation = StackNavigator(
	{
		SignInScreen,
	},
	{
		initialRouteName: 'SignInScreen',
		headerMode: 'none',
	},
);

const RootNavigation = SwitchNavigator(
	{
		LoadingScreen,
		App: appDrawerNavigation,
		Auth: authNavigation,
	},
	{
		initialRouteName: 'LoadingScreen',
	},
);

export default class App extends React.Component<{}> {

	recipeStore: RecipeStore;

	constructor(props: {}) {
		super(props);

		Parse.setAsyncStorage(AsyncStorage);
		Parse.User.enableUnsafeCurrentUser();
		(Parse.Object as any).disableSingleInstance();
		Parse.initialize('35bc3795-115a-4ae7-b3f7-db636f9fddb0', 'W0GRtdeEtaosJEjKZvUpqzFtATpaIudc');
		Parse.serverURL = 'https://api.parse.buddy.com/parse';

		this.recipeStore = new RecipeStore();
	}

	render() {
		return (
			<Provider
				recipeStore={this.recipeStore}
			>
				<RootNavigation />
			</Provider>
		);
	}
}
