import React from 'react';
import Parse from 'parse/react-native';
import { inject } from 'mobx-react';
import { Keyboard, Alert } from 'react-native';
import { Container, Button, Content, Form, Item, Input, Text, Label, Spinner } from 'native-base';
import { NavigationScreenProp } from 'react-navigation';

import RecipeStore from '../../stores/RecipeStore';

type Props = { navigation: NavigationScreenProp<{}>, recipeStore: RecipeStore };

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
class SignInScreen extends React.Component<Props, { loading: boolean }> {

	constructor(props: Props) {
		super(props);

		this.state = {
			loading: false,
		};
	}

	inputs = {
		email: '',
		password: '',
	};

	onSignIn() {
		if (!this.inputs.email || !this.inputs.password) {
			return;
		}
		
		this.setState({ loading: true });
		Keyboard.dismiss();

		Promise.resolve(Parse.User.logIn(this.inputs.email, this.inputs.password))
			.then(() => {
				this.props.recipeStore.load()
					.then(() => {
						this.setState({ loading: false });
						this.props.navigation.navigate('App');
					})
					.catch(err => {
						Alert.alert(
							'Error loading recipes',
							err.message,
							[
								{ text: 'OK' },
							],
						);
						this.props.navigation.navigate('App');
					});
			})
			.catch(err => {
				Alert.alert(
					'Error signing in',
					err.message,
					[
						{ text: 'OK' },
					],
				);
				this.setState({ loading: false });
			});
	}

	render() {
		return (
			<Container>
				<Content padder>
					<Form>
						<Item floatingLabel>
							<Label>Email Address</Label>
							<Input
								keyboardType="email-address"
								autoFocus={true}
								onChangeText={value => this.inputs.email = value}
							/>
						</Item>

						<Item floatingLabel>
							<Label>Password</Label>
							<Input
								secureTextEntry={true}
								onChangeText={value => this.inputs.password = value}
							/>
						</Item>
					</Form>
					<Button
						block
						primary
						style={{ marginTop: 30 }}
						onPress={this.onSignIn.bind(this)}
						disabled={this.state.loading}
					>
						<Text>Sign In</Text>
					</Button>
					{this.state.loading && <Spinner color="blue" />}
				</Content>
			</Container>
		);
	}
}

export default SignInScreen;
