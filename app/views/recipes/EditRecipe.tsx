import React from 'react';
import { observer, inject } from 'mobx-react';
import { Keyboard, Alert } from 'react-native';
import { NavigationScreenProp, NavigationScreenConfigProps } from 'react-navigation';
import { Container, Body, Content, Header, Left, Right, Icon, Title, Button, Text,
	Form, Item, Label, Input, Picker, Spinner } from 'native-base';

import RecipeStore from '../../stores/RecipeStore';
import Recipe from '../../models/Recipe';

type Props = {
	navigation: NavigationScreenProp<{}, { recipeId: string }>,
	recipeStore: RecipeStore,
};

type State = {
	loading: boolean,
	name: string,
	style: string,
	batchSize: string,
	boilSize: string,
	boilTime: string,
};

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
@observer
class EditRecipe extends React.Component<Props, State> {

	static navigationOptions = (props: NavigationScreenConfigProps) => ({
		header: (
			<Header>
				<Left>
					<Button
						transparent
						onPress={() => props.navigation.goBack()}>
						<Icon name="arrow-back" />
					</Button>
				</Left>
				<Body>
					<Title>Recipe Details</Title>
				</Body>
				<Right />
			</Header>
		),
	})

	private recipe: Recipe;

	constructor(props: Props) {
		super(props);

		this.state = {
			loading: false,
			name: '',
			style: '',
			batchSize: '',
			boilSize: '',
			boilTime: '',
		};
	}

	componentDidMount() {
		this.recipe = this.props.recipeStore.getById(this.props.navigation.getParam('recipeId'));

		this.setState({
			name: this.recipe.name,
			style: this.recipe.style,
			batchSize: this.recipe.batchSize ? this.recipe.batchSize.toString() : '',
			boilSize: this.recipe.boilSize ? this.recipe.boilSize.toString() : '',
			boilTime: this.recipe.boilTime ? this.recipe.boilTime.toString() : '',
		});
	}

	onSave() {
		this.setState({ loading: true });
		Keyboard.dismiss();

		this.recipe.name = this.state.name;
		this.recipe.style = this.state.style;
		this.recipe.batchSize = parseFloat(this.state.batchSize);
		this.recipe.boilSize = parseFloat(this.state.boilSize);
		this.recipe.boilTime = parseFloat(this.state.boilTime);

		this.props.recipeStore.saveRecipe(this.recipe)
			.then(() => {
				this.setState({ loading: false });
				this.props.navigation.goBack();
			})
			.catch(err => {
				Alert.alert(
					'Error updating recipe',
					err.message,
					[
						{ text: 'OK' },
					],
				);
				this.setState({ loading: false });
			});
	}

	render() {
		return (
			<Container>
				<Content padder>
					<Form>
						<Item floatingLabel>
							<Label>Name</Label>
							<Input
								autoFocus={true}
								value={this.state.name}
								onChangeText={value => this.setState({ name: value })}
							/>
						</Item>

						<Item>
							<Label>Beer Style</Label>
							<Picker
								placeholder="Beer Style"
								mode="dropdown"
								selectedValue={this.state.style}
								onValueChange={value => this.setState({ style: value })}
							>
								<Picker.Item label="Light Ale" value="Light Ale" />
								<Picker.Item label="Dark Ale" value="Dark Ale" />
								<Picker.Item label="India Pale Ale" value="India Pale Ale" />
								<Picker.Item label="American Pale Ale" value="American Pale Ale" />
							</Picker>
						</Item>

						<Item floatingLabel>
							<Label>Batch Size</Label>
							<Input
								autoFocus={true}
								value={this.state.batchSize}
								onChangeText={value => this.setState({ batchSize: value })}
							/>
						</Item>

						<Item floatingLabel>
							<Label>Boil Size</Label>
							<Input
								autoFocus={true}
								value={this.state.boilSize}
								onChangeText={value => this.setState({ boilSize: value })}
							/>
						</Item>

						<Item floatingLabel>
							<Label>Boil Time</Label>
							<Input
								autoFocus={true}
								value={this.state.boilTime}
								onChangeText={value => this.setState({ boilTime: value })}
							/>
						</Item>
					</Form>

					<Button
						block
						primary
						style={{ marginTop: 30 }}
						onPress={this.onSave.bind(this)}
						disabled={this.state.loading}
					>
						<Text>Update Recipe</Text>
					</Button>
					{this.state.loading && <Spinner color="blue" />}
				</Content>
			</Container>
		);
	}
}

export default EditRecipe;
