import React from 'react';
import { inject, observer } from 'mobx-react';
import { View, FlatList, TouchableNativeFeedback } from 'react-native';
import { NavigationScreenProp, NavigationScreenConfigProps } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text } from 'native-base';

import RecipeStore from '../../stores/RecipeStore';
import Recipe from '../../models/Recipe';

class RecipeFlatList extends FlatList<Recipe> { }

type Props = { navigation: NavigationScreenProp<{}>, recipeStore: RecipeStore };

@observer
class RecipeItem extends React.Component<{ recipe: Recipe, onPress: (recipe: Recipe) => void }> {
	render() {
		const onPress = this.props.onPress;

		return (
			<TouchableNativeFeedback
				onPress={() => onPress(this.props.recipe)}
			>
				<View>
					<Text>
						{this.props.recipe.name}
					</Text>
					<Text>
						{this.props.recipe.style}
					</Text>
				</View>
			</TouchableNativeFeedback>
		);
	}
}

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
@observer
class RecipeList extends React.Component<Props, { showModal: boolean }> {

	static navigationOptions = (props: NavigationScreenConfigProps) => ({
		header: (
			<Header>
				<Left>
					<Button
						transparent
						onPress={() => props.navigation.navigate('DrawerOpen')}>
						<Icon name="menu" />
					</Button>
				</Left>
				<Body>
					<Title>Recipes</Title>
				</Body>
				<Right />
			</Header>
		),
	})

	constructor(props: Props) {
		super(props);

		this.state = {
			showModal: false,
		};
	}

	onPressItem(recipe: Recipe) {
		this.props.navigation.navigate('ViewRecipeScreen', { recipeId: recipe.id });
	}

	openModal() {
		this.props.navigation.navigate('NewRecipeScreen');
	}

	render() {
		const recipes: Recipe[] = this.props.recipeStore.recipes;

		return (
			<Container>
				<Content padder>
					<RecipeFlatList
						data={recipes.slice()}
						keyExtractor={recipe => recipe.id}
						renderItem={({ item }) =>
							<RecipeItem
								recipe={item}
								onPress={this.onPressItem.bind(this)}>
							</RecipeItem>}
					/>
				</Content>

				<ActionButton
					buttonColor="#5067FF"
					onPress={this.openModal.bind(this)}
				/>
			</Container>
		);
	}
}

export default RecipeList;
