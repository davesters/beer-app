import React from 'react';
import { observer, inject } from 'mobx-react';
import { View, FlatList, StyleSheet } from 'react-native';
import { NavigationScreenProp, NavigationScreenConfigProps } from 'react-navigation';
import ActionButton from 'react-native-action-button';
import { Container, Body, Content, Header, Left, Right, Icon, Title, Button, Text } from 'native-base';

import RecipeStore from '../../stores/RecipeStore';
import Recipe from '../../models/Recipe';
import WaterUnit from '../../models/WaterUnit';
import NewIngredientModal from '../../components/NewIngredientModal';
import RecipeIngredient from '../../models/RecipeIngredient';
import IngredientType from '../../models/IngredientType';

class IngredientFlatList extends FlatList<RecipeIngredient> { }

type Props = {
	navigation: NavigationScreenProp<{}, { recipeId: string }>,
	recipeStore: RecipeStore,
};

type State = {
	loading: boolean,
	showModal: boolean,
	ingredientToAddType: IngredientType,
};

@observer
class IngredientItem extends React.Component<{ ingredient: RecipeIngredient }> {
	render() {
		return (
			<View>
				<Text>
					Name: {this.props.ingredient.name}
				</Text>
				<Text>
					Type: {this.props.ingredient.type}
				</Text>
				<Text>
					Amount: {this.props.ingredient.amount}
				</Text>
			</View>
		);
	}
}

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
@observer
class ViewRecipe extends React.Component<Props, State> {

	static navigationOptions = (props: NavigationScreenConfigProps) => ({
		header: (
			<Header>
				<Left>
					<Button
						transparent
						onPress={() => props.navigation.goBack()}>
						<Icon name="arrow-back" />
					</Button>
				</Left>
				<Body>
					<Title>Recipe Details</Title>
				</Body>
				<Right>
					<Button
						transparent
						onPress={() => props.navigation.navigate('EditRecipeScreen', {
							recipeId: props.navigation.getParam('recipeId'),
						})}>
						<Text>Edit</Text>
					</Button>
				</Right>
			</Header>
		),
	})

	constructor(props: Props) {
		super(props);

		this.state = {
			loading: false,
			showModal: false,
			ingredientToAddType: IngredientType.Other,
		};
	}

	openModal(type: IngredientType) {
		this.setState({ showModal: true, ingredientToAddType: type });
	}

	closeModal() {
		this.setState({ showModal: false });
	}

	render() {
		const recipe: Recipe = this.props.recipeStore.getById(this.props.navigation.getParam('recipeId'));

		return (
			<Container>
				<Content padder>
					<NewIngredientModal
						modalVisible={this.state.showModal}
						closeModal={this.closeModal.bind(this)}
						recipeId={this.props.navigation.getParam('recipeId')}
						type={this.state.ingredientToAddType}
					/>

					<Text>
						Name: {recipe.name}
					</Text>
					<Text>
						Type: {recipe.type}
					</Text>
					<Text>
						Style: {recipe.style}
					</Text>
					<Text>
						OG: {recipe.og}, FG: {recipe.fg}, ABV: {recipe.abv}, SRM: {recipe.srm}
					</Text>
					<Text>
						Batch Size: {recipe.batchSize} {recipe.waterUnits === WaterUnit.GALLON ? 'gal' : 'L'}
					</Text>
					<Text>
						Boil Size: {recipe.boilSize} {recipe.waterUnits === WaterUnit.GALLON ? 'gal' : 'L'}
					</Text>
					<Text>
						Boil Time: {recipe.boilTime}
					</Text>
					<Text>
						Notes: {recipe.notes}
					</Text>

					<IngredientFlatList
						data={recipe.ingredients.slice()}
						keyExtractor={ingredient => ingredient.id}
						renderItem={({ item }) =>
							<IngredientItem ingredient={item} />}
					/>
				</Content>

				<ActionButton buttonColor="#5067FF">
					<ActionButton.Item
						buttonColor="#9b59b6"
						title="Add Grain"
						onPress={this.openModal.bind(this, IngredientType.Grain)}
					>
						<Icon name="add" style={styles.actionButtonIcon} />
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor="#9b59b6"
						title="Add Fermentable"
						onPress={this.openModal.bind(this, IngredientType.Fermentable)}
					>
						<Icon name="add" style={styles.actionButtonIcon} />
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor="#9b59b6"
						title="Add Hop"
						onPress={this.openModal.bind(this, IngredientType.Hop)}
					>
						<Icon name="add" style={styles.actionButtonIcon} />
					</ActionButton.Item>
					<ActionButton.Item
						buttonColor="#9b59b6"
						title="Add Other"
						onPress={this.openModal.bind(this, IngredientType.Other)}
					>
						<Icon name="add" style={styles.actionButtonIcon} />
					</ActionButton.Item>
				</ActionButton>
			</Container>
		);
	}
}

const styles = StyleSheet.create({
	actionButtonIcon: {
		fontSize: 20,
		height: 22,
		color: 'white',
	},
});

export default ViewRecipe;
