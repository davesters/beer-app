import React from 'react';
import { inject } from 'mobx-react';
import { NavigationScreenProp, NavigationScreenConfigProps } from 'react-navigation';
import { Keyboard, Alert } from 'react-native';
import { Container, Header, Title, Left, Icon, Right, Button, Body, Content, Text,
	Spinner, Form, Input, Item, Label, Picker } from 'native-base';

import RecipeStore from '../../stores/RecipeStore';
import Recipe from '../../models/Recipe';
import RecipeType from '../../models/RecipeType';

interface Props {
	navigation: NavigationScreenProp<{}, { recipeId: string }>,
	recipeStore?: RecipeStore;
	modalVisible: boolean;
}

interface State {
	loading: boolean;
	name: string;
	style: string;
}

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
class NewRecipeModal extends React.Component<Props, State> {

	static navigationOptions = (props: NavigationScreenConfigProps) => ({
		header: (
			<Header>
				<Left>
					<Button
						transparent
						onPress={() => props.navigation.goBack()}>
						<Icon name="arrow-back" />
					</Button>
				</Left>
				<Body>
					<Title>Create New Recipe</Title>
				</Body>
				<Right />
			</Header>
		),
	})

	constructor(props: Props) {
		super(props);

		this.state = {
			loading: false,
			name: '',
			style: '',
		};
	}

	onCreate() {
		this.setState({ loading: true });
		Keyboard.dismiss();

		const recipe = new Recipe();
		recipe.name = this.state.name;
		recipe.style = this.state.style;
		recipe.type = RecipeType.Extract;

		this.props.recipeStore.saveRecipe(recipe)
			.then(() => {
				this.setState({ loading: false });
				this.props.navigation.goBack();
			})
			.catch(err => {
				Alert.alert(
					'Error saving recipe',
					err.message,
					[
						{ text: 'OK' },
					],
				);
				this.setState({ loading: false });
			});
	}

	render() {
		return (
			<Container>
				<Content padder>
					<Form>
						<Item floatingLabel>
							<Label>Recipe Name</Label>
							<Input
								autoFocus={true}
								onChangeText={value => this.setState({ name: value })}
							/>
						</Item>

						<Item>
							<Label>Beer Style</Label>
							<Picker
								placeholder="Beer Style"
								mode="dropdown"
								selectedValue={this.state.style}
								onValueChange={value => this.setState({ style: value })}
							>
								<Picker.Item label="Light Ale" value="Light Ale" />
								<Picker.Item label="Dark Ale" value="Dark Ale" />
								<Picker.Item label="India Pale Ale" value="India Pale Ale" />
								<Picker.Item label="American Pale Ale" value="American Pale Ale" />
							</Picker>
						</Item>
					</Form>

					<Button
						block
						primary
						style={{ marginTop: 30 }}
						onPress={this.onCreate.bind(this)}
						disabled={this.state.loading}
					>
						<Text>Create Recipe</Text>
					</Button>
					{this.state.loading && <Spinner color="blue" />}
				</Content>
			</Container>
		);
	}
}

export default NewRecipeModal;
