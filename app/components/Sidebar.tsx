import React from 'react';
import Parse from 'parse/react-native';
import { inject } from 'mobx-react';
import { Image } from 'react-native';
import { NavigationScreenProp, NavigationActions } from 'react-navigation';
import { Container, Content, Text, List, ListItem } from 'native-base';

import RecipeStore from '../stores/RecipeStore';

interface Props {
	navigation: NavigationScreenProp<{}>;
	rootNavigation: NavigationScreenProp<{}>;
	recipeStore: RecipeStore;
}

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
class Sidebar extends React.Component<Props> {
	render() {
		return (
			<Container>
				<Content>
					<Image
						source={{
							uri: 'https://raw.githubusercontent.com/GeekyAnts/NativeBase-KitchenSink/master/assets/drawer-cover.png',
						}}
						style={{
							height: 120,
							alignSelf: 'stretch',
							justifyContent: 'center',
							alignItems: 'center',
						}}
					/>

					<List>
						<ListItem>
							<Text
								onPress={() => this.props.navigation.dispatch(NavigationActions.reset({
									index: 0,
									actions: [NavigationActions.navigate({ routeName: 'RecipeListScreen' })],
								}))}>
								Recipes
							</Text>
						</ListItem>

						<ListItem>
							<Text onPress={() => {
								Parse.User.logOut()
									.then(() => {
										this.props.recipeStore.clear();
										this.props.rootNavigation.navigate('Auth');
									});
							}}>
								Sign Out
							</Text>
						</ListItem>
					</List>
				</Content>
			</Container>
		);
	}
}

export default Sidebar;
