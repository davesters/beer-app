import React from 'react';
import { inject } from 'mobx-react';
import { KeyboardAvoidingView, Modal, Text, View, TextInput, Button, Keyboard, Alert } from 'react-native';

import RecipeStore from '../stores/RecipeStore';
import RecipeIngredient from '../models/RecipeIngredient';
import IngredientType from '../models/IngredientType';

interface Props {
	recipeStore?: RecipeStore;
	modalVisible: boolean;
	type: IngredientType;
	recipeId: string;
	closeModal: () => void;
}

interface State {
	loading: boolean;
	name: string;
	amount: string;
}

@inject((stores: any) => ({ recipeStore: stores.recipeStore as RecipeStore }))
class NewIngredientModal extends React.Component<Props, State> {

	constructor(props: Props) {
		super(props);

		this.state = {
			loading: false,
			name: '',
			amount: '',
		};
	}

	onAdd() {
		this.setState({ loading: true });
		Keyboard.dismiss();

		const ingredient = new RecipeIngredient();
		ingredient.name = this.state.name;
		ingredient.amount = parseFloat(this.state.amount);
		ingredient.type = this.props.type;

		this.props.recipeStore.addIngredientToRecipe(ingredient, this.props.recipeId)
			.then(() => {
				this.setState({ loading: false });
				this.props.closeModal();
			})
			.catch(err => {
				Alert.alert(
					'Error adding ingredient to recipe',
					err.message,
					[
						{ text: 'OK' },
					],
				);
				this.setState({ loading: false });
			});
	}

	render() {
		let title = '';

		switch (this.props.type) {
		case IngredientType.Grain:
			title = 'Grain';
			break;
		case IngredientType.Fermentable:
			title = 'Fermentable';
			break;
		case IngredientType.Hop:
			title = 'Hop';
			break;
		case IngredientType.Other:
			title = 'Other Ingredient';
			break;
		}

		return (
			<Modal
				animationType="slide"
				transparent={false}
				visible={this.props.modalVisible}
				onRequestClose={() => this.props.closeModal()}
			>
				<KeyboardAvoidingView style={{ marginTop: 25 }} behavior="padding">
					<Text>Add {title}</Text>
					<TextInput
						placeholder="Name"
						autoFocus={true}
						onChangeText={value => this.setState({ name: value })} />

					<Text>Amount</Text>
					<TextInput
						placeholder="Amount"
						onChangeText={value => this.setState({ amount: value })} />

					<Button
						title="Add Ingredient"
						onPress={this.onAdd.bind(this)}
						disabled={this.state.loading} />

					<Button
						title="Cancel"
						onPress={() => this.props.closeModal()} />

					<View style={{ height: 60 }} />
				</KeyboardAvoidingView>
			</Modal>
		);
	}
}

export default NewIngredientModal;
