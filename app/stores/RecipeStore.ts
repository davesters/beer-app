import { observable, action, runInAction, configure } from 'mobx';
import Parse from 'parse/react-native';

import Recipe from '../models/Recipe';
import RecipeIngredient from '../models/RecipeIngredient';

configure({ enforceActions: 'strict' });

class RecipeStore {

	@observable
	public recipes: Recipe[];

	constructor() {
		runInAction(() => {
			this.recipes = [];
		});
	}

	public load(): Promise<void> {
		return Promise.resolve(new Parse.Query(Recipe).descending('updatedAt').find())
			.then(recipes => {
				this.clear();

				recipes.forEach(action('loadRecipe', (recipe: Parse.Object) => {
					this.recipes.push(Recipe.fromJSON(recipe.toJSON()));
					this.loadIngredients(recipe.id);
				}));
			});
	}

	public getById(id: string) {
		return this.recipes.find(r => r.id === id);
	}

	@action
	public clear() {
		this.recipes.slice(0, 0);
		this.recipes.pop();
	}

	public loadIngredients(recipeId: string): Promise<void> {
		const recipe = new Recipe();
		recipe.id = recipeId;
		const query = new Parse.Query(RecipeIngredient)
			.equalTo('recipe', recipe)
			.find();

		return Promise.resolve(query)
			.then(ingredients => {
				const recipeIndex = this.recipes.findIndex(r => r.id === recipeId);
				
				ingredients.forEach(action('loadIngredient', (i: Parse.Object) => {
					this.recipes[recipeIndex].addIngredient(RecipeIngredient.fromJSON(i.toJSON()));
				}));
			});
	}

	public saveRecipe(recipe: Recipe): Promise<void> {
		return Promise.resolve(Parse.User.currentAsync())
			.then(user => {
				const acl = new Parse.ACL();
				acl.setPublicReadAccess(false);
				acl.setPublicWriteAccess(false);
				acl.setReadAccess(user, true);
				acl.setWriteAccess(user, true);
				recipe.setACL(acl);

				return Promise.resolve(recipe.save(null))
					.then(action('savedRecipe', (saved: Recipe) => {
						const index = this.recipes.findIndex(r => r.id === saved.id);
						const savedRecipe = Recipe.fromJSON(saved.toJSON());

						if (index > -1) {
							this.recipes[index] = savedRecipe;
						} else {
							this.recipes.unshift(savedRecipe);
						}
					}));
			});
	}

	public addIngredientToRecipe(ingredient: RecipeIngredient, recipeId: string): Promise<void> {
		const recipe = new Recipe();
		recipe.id = recipeId;

		ingredient.recipe = recipe;

		return Promise.resolve(Parse.User.currentAsync())
			.then(user => {
				const acl = new Parse.ACL();
				acl.setPublicReadAccess(false);
				acl.setPublicWriteAccess(false);
				acl.setReadAccess(user, true);
				acl.setWriteAccess(user, true);
				ingredient.setACL(acl);

				return Promise.resolve(ingredient.save())
					.then(action('addIngredient', (i: RecipeIngredient) => {
						const recipeIndex = this.recipes.findIndex(r => r.id === recipeId);
						this.recipes[recipeIndex].addIngredient(RecipeIngredient.fromJSON(i.toJSON()));
					}));
			});
	}
}

export default RecipeStore;
