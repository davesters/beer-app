
enum WaterUnit {
	LITER = 'L',
	GALLON = 'G',
}

export default WaterUnit;
