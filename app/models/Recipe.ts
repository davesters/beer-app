import * as Parse from 'parse/react-native';
import { computed, observable, action, runInAction } from 'mobx';

import RecipeType from './RecipeType';
import RecipeIngredient from './RecipeIngredient';
import WaterUnit from './WaterUnit';

export default class Recipe extends Parse.Object {

	@observable
	ingredients: RecipeIngredient[];

	constructor() {
		super('Recipe');

		runInAction(() => this.ingredients = []);
	}

	static fromJSON(json: any): Recipe {
		const recipe = new Recipe();

		Object.keys(json).forEach(key => {
			recipe.set(key, json[key]);
		});
		
		return recipe;
	}

	@computed get name(): string {
		return this.get('name');
	}
	set name(value: string) {
		this.set('name', value);
	}

	@computed get type(): RecipeType {
		return this.get('type');
	}
	set type(value: RecipeType) {
		this.set('type', value);
	}

	@computed get author(): string {
		return this.get('author');
	}
	set author(value: string) {
		this.set('author', value);
	}

	@computed get style(): string {
		return this.get('style');
	}
	set style(value: string) {
		this.set('style', value);
	}

	@computed get waterUnits(): WaterUnit {
		return this.get('waterUnits');
	}
	set waterUnits(value: WaterUnit) {
		this.set('waterUnits', value);
	}

	@computed get batchSize(): number {
		return this.get('batchSize');
	}
	set batchSize(value: number) {
		this.set('batchSize', value);
	}

	@computed get boilSize(): number {
		return this.get('boilSize');
	}
	set boilSize(value: number) {
		this.set('boilSize', value);
	}

	@computed get boilTime(): number {
		return this.get('boilTime');
	}
	set boilTime(value: number) {
		this.set('boilTime', value);
	}

	@computed get og(): number {
		return this.get('og');
	}
	set og(value: number) {
		this.set('og', value);
	}

	@computed get fg(): number {
		return this.get('fg');
	}
	set fg(value: number) {
		this.set('fg', value);
	}

	@computed get abv(): number {
		return this.get('abv');
	}
	set abv(value: number) {
		this.set('abv', value);
	}

	@computed get ibu(): number {
		return this.get('ibu');
	}
	set ibu(value: number) {
		this.set('ibu', value);
	}

	@computed get srm(): number {
		return this.get('srm');
	}
	set srm(value: number) {
		this.set('srm', value);
	}

	@computed get notes(): string {
		return this.get('notes');
	}
	set notes(value: string) {
		this.set('notes', value);
	}

	@action
	public addIngredient(ingredient: RecipeIngredient) {
		this.ingredients.push(ingredient);
	}
}
