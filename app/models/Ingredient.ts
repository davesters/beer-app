import * as Parse from 'parse/react-native';
import { computed } from 'mobx';

import IngredientType from './IngredientType';

export default class Ingredient extends Parse.Object {
	constructor() {
		super('ingredient');
	}

	@computed get name(): string {
		return this.get('name');
	}
	set name(value: string) {
		this.set('name', value);
	}

	@computed get description(): string {
		return this.get('desc');
	}
	set description(value: string) {
		this.set('desc', value);
	}

	@computed get type(): IngredientType {
		return this.get('type');
	}
	set type(value: IngredientType) {
		this.set('type', value);
	}
}
