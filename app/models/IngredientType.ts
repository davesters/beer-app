
enum IngredientType {
	Grain = 'Grain',
	Fermentable = 'Fermentable',
	Hop = 'Hop',
	Other = 'Other',
}

export default IngredientType;
