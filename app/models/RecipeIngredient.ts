import * as Parse from 'parse/react-native';
import { computed } from 'mobx';

import Recipe from './Recipe';
import Ingredient from './Ingredient';
import IngredientType from './IngredientType';

export default class RecipeIngredient extends Parse.Object {
	constructor() {
		super('recipe_ingredient');
	}

	static fromJSON(json: any): RecipeIngredient {
		const ingredient = new RecipeIngredient();

		Object.keys(json).forEach(key => {
			ingredient.set(key, json[key]);
		});

		return ingredient;
	}

	@computed get name(): string {
		return this.get('name');
	}
	set name(value: string) {
		this.set('name', value);
	}

	@computed get type(): IngredientType {
		return this.get('type');
	}
	set type(value: IngredientType) {
		this.set('type', value);
	}

	@computed get amount(): number {
		return this.get('amount');
	}
	set amount(value: number) {
		this.set('amount', value);
	}

	@computed get recipe(): Recipe {
		return this.get('recipe');
	}
	set recipe(value: Recipe) {
		this.set('recipe', value);
	}

	@computed get ingredient(): Ingredient {
		return this.get('ingredient');
	}
	set ingredient(value: Ingredient) {
		this.set('ingredient', value);
	}
}
