
enum RecipeType {
	Extract = 'Extract',
	PartialGrain = 'Partial Grain',
	FullGrain = 'Full Grain',
}

export default RecipeType;
